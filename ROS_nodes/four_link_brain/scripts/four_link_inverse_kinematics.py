#!/usr/bin/env python
import rospy
import math
import tf
from visualization_msgs.msg import Marker
from sensor_msgs.msg import JointState
from std_msgs.msg import Header
from geometry_msgs.msg import Pose2D
# robot module import
import four_link_solver as four_link

# robot module import
import four_link_solver as robot


def show_marker_dk(pose):
    marker = Marker()
    marker.header.frame_id = "base_link"
    marker.pose.orientation.w = 1.0
    marker.pose.position.x = pose[3]
    marker.pose.position.y = pose[4]
    marker.pose.position.z = pose[5]
    quaternion = tf.transformations.quaternion_from_euler(pose[0], pose[1], pose[2])
    #type(pose) = geometry_msgs.msg.Pose
    marker.pose.orientation.x = quaternion[0]
    marker.pose.orientation.y = quaternion[1]
    marker.pose.orientation.z = quaternion[2]
    marker.pose.orientation.w = quaternion[3]

    marker.type = marker.CUBE
    marker.color.g = 1.0
    marker.color.a = 0.5
    marker.scale.x = 0.05
    marker.scale.y = 0.05
    marker.scale.z = 0.05

    marker_dk = rospy.Publisher('/four_link/direct_kinematics', Marker, queue_size=10)
    marker_dk.publish(marker)
    
def show_marker_ik(pose):

    marker = Marker()
    marker.header.frame_id = "base_link"
    marker.pose.orientation.w = 1.0
    marker.pose.position.x = pose[3]
    marker.pose.position.y = pose[4]
    marker.pose.position.z = pose[5]
    quaternion = tf.transformations.quaternion_from_euler(pose[0], pose[1], pose[2])
    #type(pose) = geometry_msgs.msg.Pose
    marker.pose.orientation.x = quaternion[0]
    marker.pose.orientation.y = quaternion[1]
    marker.pose.orientation.z = quaternion[2]
    marker.pose.orientation.w = quaternion[3]

    marker.type = marker.CUBE
    marker.color.r = 1.0
    marker.color.a = 0.5
    marker.scale.x = 0.05
    marker.scale.y = 0.05
    marker.scale.z = 0.05

    marker_ik = rospy.Publisher('/four_link/inverse_kinematics', Marker, queue_size=10)
    marker_ik.publish(marker)
    
def callback(data):
    global target_pose 
    target_pose = [data.theta, 0, 0, 0, data.x, data.y]


def four_link_inverse_kinematics():

    rospy.init_node('four_link_inverse_kinematics', anonymous=True)
    publish_states = rospy.Publisher("/joint_states", JointState, queue_size=10)
    rospy.Subscriber("/goto", Pose2D, callback)

    rate = rospy.Rate(10) # 10hz sampling freq
    
    #initial state
    q = [0, 0, 0, 0]
    # target pose received from /goto message
    global target_pose 
    target_pose = four_link.dk(q)
    while not rospy.is_shutdown():

        # jointstates message construct
        joints = JointState()
        joints.header = Header()
        joints.header.stamp = rospy.Time.now()
        joints.name = ["joint0","joint1","joint2","joint3","joint4","joint5"]
        joints.effort = [0, 0, 0, 0, 0, 0]
        joints.velocity = [0, 0, 0, 0, 0, 0]

        # iterative solution to inverse kinematics
        q = four_link.ik_solve(target_pose, q, 1)
        joints.position = [q[0],q[1],q[2],q[3], 0, 0]

        # publish the messqes
        publish_states.publish(joints)
        show_marker_dk(four_link.dk(q))
        show_marker_ik(target_pose)

        # wait for next sampling time 
        rate.sleep()

if __name__ == '__main__':
    try:
        four_link_inverse_kinematics()
    except rospy.ROSInterruptException:
        pass