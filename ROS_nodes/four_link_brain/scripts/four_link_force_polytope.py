#!/usr/bin/env python
import rospy
import tf
from visualization_msgs.msg import Marker
from sensor_msgs.msg import JointState, PointCloud 
from geometry_msgs.msg import Polygon, Point32, PolygonStamped
from std_msgs.msg import Header
# robot module import
import four_link_solver as four_link


# function drawing the marker upon receiving new joint positions
def callback(data):

    # calculate dk of the robot
    pose = four_link.dk(data.position)
    # calculate force vertexes
    force_vertex = four_link.force_polytope_ordered(data.position)
   
    # build polytop (polygon)
    polygon_massage = Polygon()
    pointcloud_massage = PointCloud()
    for i in range(force_vertex.shape[1]):
        point = Point32()
        point.x = pose[3]
        point.y = force_vertex[0,i] + pose[4]
        point.z = force_vertex[1,i] + pose[5]
        polygon_massage.points.append(point)
        pointcloud_massage.points.append(point)

    # polytop stamped message
    polygon_stamped = PolygonStamped()
    polygon_stamped.polygon = polygon_massage
    polygon_stamped.header = Header()
    polygon_stamped.header.frame_id = 'base_link'
    polygon_stamped.header.stamp = rospy.Time.now()
    # publish plytop
    publish_force_polytop = rospy.Publisher('/four_link/force_polytop', PolygonStamped, queue_size=10)
    publish_force_polytop.publish(polygon_stamped)

    # polytop stamped message
    pointcloud_massage.header = Header()
    pointcloud_massage.header.frame_id = 'base_link'
    pointcloud_massage.header.stamp = rospy.Time.now()
    # publish plytop
    publish_force_polytop_vertex = rospy.Publisher('/four_link/force_polytop_vertex', PointCloud, queue_size=10)
    publish_force_polytop_vertex.publish(pointcloud_massage)


# main funciton of the class
def four_link_force_polytope():

    rospy.init_node('four_link_force_polytope', anonymous=True)
    rospy.Subscriber("/joint_states", JointState, callback)

    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        rate.sleep()

# class definition
if __name__ == '__main__':
    try:
        four_link_force_polytope()
    except rospy.ROSInterruptException:
        pass