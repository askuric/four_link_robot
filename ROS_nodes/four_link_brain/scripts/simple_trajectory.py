#!/usr/bin/env python
import rospy
import math
from std_msgs.msg import Float64

def test_trajectory():
    pub_joint0 = rospy.Publisher('/four_link/joint0_position_controller/command', Float64, queue_size=10)
    pub_joint1 = rospy.Publisher('/four_link/joint1_position_controller/command', Float64, queue_size=10)
    pub_joint2 = rospy.Publisher('/four_link/joint2_position_controller/command', Float64, queue_size=10)
    pub_joint3 = rospy.Publisher('/four_link/joint3_position_controller/command', Float64, queue_size=10)
    rospy.init_node('test_trajectory', anonymous=True)
    rate = rospy.Rate(10) # 0hz
    # track time
    t = 0
    while not rospy.is_shutdown():
        traj_k = 0.5*math.sin(0.1*math.pi*t)
        t = t+ 0.1
        rospy.loginfo(traj_k)
        pub_joint0.publish(-traj_k)
        pub_joint1.publish(2*traj_k)
        pub_joint2.publish(2*traj_k)
        pub_joint3.publish(2*traj_k)
        rate.sleep()

if __name__ == '__main__':
    try:
        test_trajectory()
    except rospy.ROSInterruptException:
        pass