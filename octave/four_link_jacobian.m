syms q1 q2 q3 q4 real
syms l0 l1 l2 l3 l4 real

# direct kinematics equations
roll = q1+q2+q3+q4;
x = 0;
y = -l1*sin(q1) - l2*sin(q1+q2) - l3*sin(q1+q2+q3) - l4*sin(q1+q2+q3+q4);
z = l0 + l1*cos(q1) + l2*cos(q1+q2) + l3*cos(q1+q2+q3) + l4*cos(q1+q2+q3+q4);
dk = [roll, 0, 0, x, y z];

# symbolic jacobian calculation
J = [diff(dk,q1)', diff(dk,q2)',diff(dk,q3)',diff(dk,q4)'];
