## Four link planar robot dynamics derivation script
# created 05.08.2020
# 
# Script uses Lagrange method to calculate th 

# simbolic variables
syms l1 l2 l3 l4 real;
syms r1 r2 r3 r4 real;
syms m1 m2 m3 m4 real;
syms q1 q2 q3 q4 real;
syms dq1 dq2 dq3 dq4 real;
syms g real;

# kinetic energy calculation
# link 1
disp('link 1 - kin');
K1 = 0.5*m1*r1^2*dq1^2;
# link 2
disp('link 2 - kin');
K2 = ( (l1*cos(q1)+r2*cos(q1+q2))*dq1 + r2*cos(q1+q2)*dq2 )^2 + ( (l1*sin(q1)+r2*sin(q1+q2))*dq1 + r2*sin(q1+q2)*dq2 )^2;
K2 = 0.5*m2*simplify(K2);
# link 3
disp('link 3 - kin');
K3 = ( (l1*cos(q1)+l2*cos(q1+q2)+r3*cos(q1+q2+q3))*dq1 + (l2*cos(q1+q2)+r3*cos(q1+q2+q3))*dq2 +(r3*cos(q1+q2+q3))*dq3 )^2 + ( (l1*sin(q1)+l2*sin(q1+q2)+r3*sin(q1+q2+q3))*dq1 + (l2*sin(q1+q2)+r3*sin(q1+q2+q3))*dq2 +(r3*sin(q1+q2+q3))*dq3 )^2;
K3 = 0.5*m3*simplify(expand(K3));
# link 4
disp('link 4 - kin');
K4 = ( (l1*cos(q1)+l2*cos(q1+q2)+l3*cos(q1+q2+q3)+r4*cos(q1+q2+q3+q4))*dq1 + (l2*cos(q1+q2)+l3*cos(q1+q2+q3)+r4*cos(q1+q2+q3+q4))*dq2 +(l3*cos(q1+q2+q3)+r4*cos(q1+q2+q3+q4))*dq3 + r4*cos(q1+q2+q3+q4)*dq4 )^2 + ( (l1*sin(q1)+l2*sin(q1+q2)+l3*sin(q1+q2+q3)+r4*sin(q1+q2+q3+q4))*dq1 + (l2*sin(q1+q2)+l3*sin(q1+q2+q3)+r4*sin(q1+q2+q3+q4))*dq2 +(l3*sin(q1+q2+q3)+r4*sin(q1+q2+q3+q4))*dq3 +r4*sin(q1+q2+q3+q4)*dq4 )^2;
K4 = 0.5*m4*simplify(expand(K4));

# potential energy
disp('potential energy');
#link 1
P1 = m1*g*r1*sin(q1);
#link 2
P2 = m2*g*(l1*sin(q1) + r2*sin(q1+q2));
#link 3
P3 = m3*g*(l1*sin(q1) + l2*sin(q1+q2) + r3*sin(q1+q2+q3));
#link 3
P4 = m4*g*(l1*sin(q1) + l2*sin(q1+q2) + l3*sin(q1+q2+q3)+ r4*sin(q1+q2+q3+q4));

# Lagrangian
disp('lagrangian claculation')

# four link robot lagrange function
L = expand(K1+K2+K3+K4 -(P1+P2+P3+P4));
# two link robot - just to be sure it works well
#L = expand(K1+K2 -(P1+P2));

# derivation dL/dq
disp('derivation dL/dq')
dL_dq1 = diff(L,q1);
dL_dq2 = diff(L,q2);
dL_dq3 = diff(L,q3);
dL_dq4 = diff(L,q4);

# derivation dL/ddq
disp('derivation dL/ddq')
dL_ddq1 = diff(L,dq1);
dL_ddq2 = diff(L,dq2);
dL_ddq3 = diff(L,dq3);
dL_ddq4 = diff(L,dq4);

## time derivation
disp('derivation d/dt(dL/ddq)')
syms a1(t) a2(t) a3(t) a4(t) da1(t) da2(t) da3(t) da4(t)
syms syms q1 q2 q3 q4 dq1 dq2 dq3 dq4 ddq1 ddq2 ddq3 ddq4 real;

# joint 1
disp('q1')
der1 = diff(subs(dL_ddq1, {q1 q2 q3 q4 dq1 dq2 dq3 dq4}, {a1(t) a2(t) a3(t) a4(t) da1(t) da2(t) da3(t) da4(t)}),t);
dL_ddq1_dt = subs(der1, { diff(a1(t),t) diff(a2(t),t) diff(a3(t),t) diff(a4(t),t) diff(da1(t),t) diff(da2(t),t) diff(da3(t),t) diff(da4(t),t) a1(t) a2(t) a3(t) a4(t) da1(t) da2(t) da3(t) da4(t)}, {dq1 dq2 dq3 dq4 ddq1 ddq2 ddq3 ddq4 q1 q2 q3 q4 dq1 dq2 dq3 dq4 });

# joint 2
disp('q2')
der2 = diff(subs(dL_ddq2, {q1 q2 q3 q4 dq1 dq2 dq3 dq4}, {a1(t) a2(t) a3(t) a4(t) da1(t) da2(t) da3(t) da4(t)}),t);
dL_ddq2_dt = subs(der2, { diff(a1(t),t) diff(a2(t),t) diff(a3(t),t) diff(a4(t),t) diff(da1(t),t) diff(da2(t),t) diff(da3(t),t) diff(da4(t),t) a1(t) a2(t) a3(t) a4(t) da1(t) da2(t) da3(t) da4(t)}, {dq1 dq2 dq3 dq4 ddq1 ddq2 ddq3 ddq4 q1 q2 q3 q4 dq1 dq2 dq3 dq4 });

# joint 3
disp('q3')
der3 = diff(subs(dL_ddq3, {q1 q2 q3 q4 dq1 dq2 dq3 dq4}, {a1(t) a2(t) a3(t) a4(t) da1(t) da2(t) da3(t) da4(t)}),t);
dL_ddq3_dt = subs(der3, { diff(a1(t),t) diff(a2(t),t) diff(a3(t),t) diff(a4(t),t) diff(da1(t),t) diff(da2(t),t) diff(da3(t),t) diff(da4(t),t) a1(t) a2(t) a3(t) a4(t) da1(t) da2(t) da3(t) da4(t)}, {dq1 dq2 dq3 dq4 ddq1 ddq2 ddq3 ddq4 q1 q2 q3 q4 dq1 dq2 dq3 dq4 });

# joint 4
disp('q4')
der4 = diff(subs(dL_ddq4, {q1 q2 q3 q4 dq1 dq2 dq3 dq4}, {a1(t) a2(t) a3(t) a4(t) da1(t) da2(t) da3(t) da4(t)}),t);
dL_ddq4_dt = subs(der4, { diff(a1(t),t) diff(a2(t),t) diff(a3(t),t) diff(a4(t),t) diff(da1(t),t) diff(da2(t),t) diff(da3(t),t) diff(da4(t),t) a1(t) a2(t) a3(t) a4(t) da1(t) da2(t) da3(t) da4(t)}, {dq1 dq2 dq3 dq4 ddq1 ddq2 ddq3 ddq4 q1 q2 q3 q4 dq1 dq2 dq3 dq4 });

# mass matrix calculation
disp('mass matrix derivation')
M11 = diff(dL_ddq1_dt,ddq1);
M12 = diff(dL_ddq1_dt,ddq2);
M13 = diff(dL_ddq1_dt,ddq3);
M14 = diff(dL_ddq1_dt,ddq4);
M21 = diff(dL_ddq2_dt,ddq1);
M22 = diff(dL_ddq2_dt,ddq2);
M23 = diff(dL_ddq2_dt,ddq3);
M24 = diff(dL_ddq2_dt,ddq4);
M31 = diff(dL_ddq3_dt,ddq1);
M32 = diff(dL_ddq3_dt,ddq2);
M33 = diff(dL_ddq3_dt,ddq3);
M34 = diff(dL_ddq3_dt,ddq4);
M41 = diff(dL_ddq4_dt,ddq1);
M42 = diff(dL_ddq4_dt,ddq2);
M43 = diff(dL_ddq4_dt,ddq3);
M44 = diff(dL_ddq4_dt,ddq4);

disp('mass matrix constructed')
M = [M11 M12 M13 M14; M21 M22 M23 M24; M31 M32 M33 M34; M41 M42 M43 M44];

# coriolis matrix
disp('coriolis matrix derivation')
C1  = subs(dL_ddq1_dt - dL_dq1,{ddq1 ddq2 ddq3 ddq4, g},{0 0 0 0 0});
C2  = subs(dL_ddq2_dt - dL_dq2,{ddq1 ddq2 ddq3 ddq4, g},{0 0 0 0 0});
C3  = subs(dL_ddq3_dt - dL_dq3,{ddq1 ddq2 ddq3 ddq4, g},{0 0 0 0 0});
C4  = subs(dL_ddq4_dt - dL_dq4,{ddq1 ddq2 ddq3 ddq4, g},{0 0 0 0 0});
disp('coriolis matrix constructed')
C = [C1;C2;C3;C4];

# gravity matrix
disp('gravity matrix derivation')
G1  = diff(dL_ddq1_dt - dL_dq1,g)*g;
G2  = diff(dL_ddq2_dt - dL_dq2,g)*g;
G3  = diff(dL_ddq3_dt - dL_dq3,g)*g;
G4  = diff(dL_ddq4_dt - dL_dq4,g)*g;
disp('gravity matrix constructed')
G = [G1;G2;G3;G4];
